package com.example.malozano.testsindela.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by malozano on 06/06/2018.
 */

public class Results implements Serializable {

    @SerializedName("channel")
    @Expose
    private Channel channel;

    /**
     * No args constructor for use in serialization
     *
     */
    public Results() {
    }

    /**
     *
     * @param channel
     */
    public Results(Channel channel) {
        super();
        this.channel = channel;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Results withChannel(Channel channel) {
        this.channel = channel;
        return this;
    }
}
