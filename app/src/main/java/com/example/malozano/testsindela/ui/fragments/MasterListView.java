package com.example.malozano.testsindela.ui.fragments;

import com.example.malozano.testsindela.model.ResponseWeather;

import java.util.List;

/**
 * Created by malozano on 04/06/2018.
 */

public interface MasterListView {
    void responseToViewLocations(ResponseWeather responseWeather);
    void showErrorConecction(String error);
    void showErrorResponse(String error);
    void showWait(boolean isShow);
}
