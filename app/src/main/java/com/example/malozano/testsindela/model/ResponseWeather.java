package com.example.malozano.testsindela.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by malozano on 06/06/2018.
 */

public class ResponseWeather implements Serializable {
    @SerializedName("query")
    @Expose
    private Query query;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseWeather() {
    }

    /**
     *
     * @param query
     */
    public ResponseWeather(Query query) {
        super();
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public ResponseWeather withQuery(Query query) {
        this.query = query;
        return this;
    }
}
