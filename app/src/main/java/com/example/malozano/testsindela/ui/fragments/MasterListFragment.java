package com.example.malozano.testsindela.ui.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.malozano.testsindela.R;
import com.example.malozano.testsindela.adapters.LocationAdapter;
import com.example.malozano.testsindela.app.App;
import com.example.malozano.testsindela.databinding.FragmentMasterListBinding;
import com.example.malozano.testsindela.model.Forecast;
import com.example.malozano.testsindela.model.ResponseWeather;
import com.example.malozano.testsindela.presenters.MasterDetailPresenter;
import com.example.malozano.testsindela.presenters.MasterDetailPresenterImpl;
import com.example.malozano.testsindela.service.ConnectionApiService;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;


public class MasterListFragment extends Fragment implements MasterListView, LocationAdapter.LocationItemListener {


    private ProgressDialog progressDialog;
    private FragmentMasterListBinding binding;
    @Inject
    ConnectionApiService connectionApiService;
    private MasterDetailPresenterImpl presenter;


    public MasterListFragment() {
        // Required empty public constructor
    }


    public static MasterListFragment newInstance() {
        MasterListFragment fragment = new MasterListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getNetworkComponent().inject(this);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        presenter = new MasterDetailPresenterImpl(this,connectionApiService);
        presenter.getWeather();
        showWait(true);
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_master_list, container, false);
        return binding.getRoot();
    }


    @Override
    public void responseToViewLocations(ResponseWeather responseWeather) {
        binding.txvNamePlace.setText(responseWeather.getQuery().getResults().getChannel().getTitle());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        LocationAdapter adapter = new LocationAdapter(responseWeather.getQuery().getResults().getChannel().getItem().getForecast(), getActivity(), this);
        binding.recyclerView.setAdapter(adapter);
    }

    @Override
    public void showErrorConecction(String error) {
        showMessage(error);
    }

    @Override
    public void showErrorResponse(String error) {
        showMessage(error);
    }


    @Override
    public void showWait(boolean isShow) {
        if (getActivity().isFinishing()) {
            return;
        }
        if (isShow) {
            progressDialog = ProgressDialog.show(getActivity(), "",
                    getString(R.string.wait_dialog), true);
            progressDialog.show();
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

        }
    }

    @Override
    public void onItemSelected(Forecast result) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, DetailFragment.newInstance(result), "");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showMessage(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                R.string.button_ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        getActivity().finish();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter!=null){
            presenter.onDestroy();
        }

    }
}
