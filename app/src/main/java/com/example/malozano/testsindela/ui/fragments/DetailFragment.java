package com.example.malozano.testsindela.ui.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.malozano.testsindela.R;
import com.example.malozano.testsindela.databinding.FragmentDetailBinding;
import com.example.malozano.testsindela.model.Forecast;


public class DetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_RESULT_DETAIL = "param1";

    private FragmentDetailBinding binding;
    private Forecast result;


    public DetailFragment() {
        // Required empty public constructor
    }


    public static DetailFragment newInstance(Forecast param1) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_RESULT_DETAIL, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            result = (Forecast) getArguments().getSerializable(ARG_RESULT_DETAIL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail, container, false);
        binding.txvDetailDate.setText(result.getDate());
        binding.txvDetailDay.setText(result.getDay());
        binding.txvDetailHigh.setText(result.getHigh());
        binding.txvDetailLow.setText(result.getLow());
        binding.txvDetailText.setText(result.getText());

        return binding.getRoot();
    }


}
