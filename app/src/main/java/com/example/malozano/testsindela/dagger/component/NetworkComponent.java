package com.example.malozano.testsindela.dagger.component;

import com.example.malozano.testsindela.dagger.module.NetworkModule;
import com.example.malozano.testsindela.ui.fragments.MasterListFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 *
 * Dagger component for inject in views
 *
 * Created by malozano on 07/06/2018.
 */
@Singleton
@Component(modules = {NetworkModule.class})
public interface NetworkComponent {
    void inject(MasterListFragment mainActivity);

}
