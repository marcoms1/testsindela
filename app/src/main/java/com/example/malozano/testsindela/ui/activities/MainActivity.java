package com.example.malozano.testsindela.ui.activities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.malozano.testsindela.R;
import com.example.malozano.testsindela.app.App;
import com.example.malozano.testsindela.ui.fragments.MasterListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment_container, MasterListFragment.newInstance(), "");
        transaction.commit();
    }
}
