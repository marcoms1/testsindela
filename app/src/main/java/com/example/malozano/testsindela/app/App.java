package com.example.malozano.testsindela.app;

import android.app.Application;

import com.example.malozano.testsindela.dagger.component.DaggerNetworkComponent;
import com.example.malozano.testsindela.dagger.component.NetworkComponent;
import com.example.malozano.testsindela.dagger.module.NetworkModule;
import com.example.malozano.testsindela.utils.Constants;

/**
 * Created by malozano on 07/06/2018.
 */

public class App extends Application {
    private NetworkComponent networkComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        networkComponent= DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule(Constants.BASE_URL))
                .build();

    }


    public NetworkComponent getNetworkComponent() {
        return networkComponent;
    }
}
