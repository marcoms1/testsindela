package com.example.malozano.testsindela.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.malozano.testsindela.R;
import com.example.malozano.testsindela.model.Forecast;

import java.util.List;

/**
 * Created by malozano on 04/06/2018.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {
    List<Forecast> resultList;
    private Context context;
    private LocationItemListener listener;

    public LocationAdapter(List<Forecast> resultList, Context context, LocationItemListener listener) {
        this.resultList = resultList;
        this.context = context;
        this.listener=listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.item_location, parent, false);

        LocationAdapter.ViewHolder viewHolder=new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txvDate.setText(resultList.get(position).getDate());
       holder.txvHigh.setText(resultList.get(position).getHigh());
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txvDate;
        TextView txvHigh;
        LinearLayout lItem;


        public ViewHolder(View itemView) {
            super(itemView);
            txvDate =itemView.findViewById(R.id.txv_date);
            txvHigh =itemView.findViewById(R.id.txv_high);
            lItem =itemView.findViewById(R.id.ll_item);
            lItem.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Forecast result = resultList.get(getAdapterPosition());
            if (listener!=null) {
                listener.onItemSelected(result);
            }
        }
    }

    public interface LocationItemListener {
        void onItemSelected(Forecast result);
    }
}
