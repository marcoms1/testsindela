package com.example.malozano.testsindela.presenters;


import com.example.malozano.testsindela.R;
import com.example.malozano.testsindela.model.ResponseWeather;
import com.example.malozano.testsindela.repository.WeatherRepository;
import com.example.malozano.testsindela.repository.WeatherRepositoryImpl;
import com.example.malozano.testsindela.service.ConnectionApiService;
import com.example.malozano.testsindela.ui.fragments.MasterListFragment;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 *
 *
 *
 * Created by malozano on 04/06/2018.
 */

public class MasterDetailPresenterImpl implements MasterDetailPresenter{
    private Subscription subscription = Subscriptions.empty();
    private final WeatherRepositoryImpl repository;
    private MasterListFragment masterListFragment;

    public MasterDetailPresenterImpl(MasterListFragment masterListFragment, ConnectionApiService connectionApiService) {
        this.masterListFragment=masterListFragment;
         repository=new WeatherRepositoryImpl(connectionApiService);
    }

    @Override
    public void getWeather() {
        String query = "select * from weather.forecast where woeid in (select woeid from geo.places(5) where text=\"nome, ak\")";
        subscription = repository.getCatalogos(query)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriberLocations());

    }

    @Override
    public void onDestroy() {

        if (subscription != null) {
            subscription.unsubscribe();
        }
    }


    private Subscriber<ResponseWeather> subscriberLocations(){
        return new Subscriber<ResponseWeather>() {
            @Override
            public void onCompleted() {
                masterListFragment.showWait(false);



            }

            @Override
            public void onError(Throwable e) {
                masterListFragment.showWait(false); //TODO
                masterListFragment.showErrorConecction(e.getMessage());

            }

            @Override
            public void onNext(ResponseWeather commonResponse) {
                if (commonResponse.getQuery().getCount()>0){
                    masterListFragment.responseToViewLocations(commonResponse);
                }else {
                    masterListFragment.showErrorResponse(masterListFragment.getString(R.string.service_error));
                }


            }
        };
    }
}
