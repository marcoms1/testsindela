package com.example.malozano.testsindela.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by malozano on 06/06/2018.
 */

public class Guid implements Serializable {
    @SerializedName("isPermaLink")
    @Expose
    private String isPermaLink;

    /**
     * No args constructor for use in serialization
     *
     */
    public Guid() {
    }

    /**
     *
     * @param isPermaLink
     */
    public Guid(String isPermaLink) {
        super();
        this.isPermaLink = isPermaLink;
    }

    public String getIsPermaLink() {
        return isPermaLink;
    }

    public void setIsPermaLink(String isPermaLink) {
        this.isPermaLink = isPermaLink;
    }

    public Guid withIsPermaLink(String isPermaLink) {
        this.isPermaLink = isPermaLink;
        return this;
    }

}
