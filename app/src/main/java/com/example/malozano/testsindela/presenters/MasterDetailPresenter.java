package com.example.malozano.testsindela.presenters;

/**
 * Created by malozano on 04/06/2018.
 */

public interface MasterDetailPresenter {

    void getWeather();
    void onDestroy();
}
