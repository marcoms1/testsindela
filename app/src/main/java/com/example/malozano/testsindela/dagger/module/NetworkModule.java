package com.example.malozano.testsindela.dagger.module;

import com.example.malozano.testsindela.service.ConnectionApiService;
import com.example.malozano.testsindela.utils.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Module dagger for inject retrofit instance
 *
 *
 * Created by malozano on 07/06/2018.
 */
@Module
public class NetworkModule {
    private String urlPath;

    public NetworkModule(String urlPath) {
        this.urlPath = urlPath;
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkhttpClient() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.interceptors().clear();
        OkHttpClient client;
        client = builder
                .addInterceptor(interceptor)
                .build();
        return client;
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl(urlPath)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }


    @Provides
    @Singleton
    public ConnectionApiService providesNetworkService(
            Retrofit retrofit) {
        return retrofit.create(ConnectionApiService.class);
    }
}
