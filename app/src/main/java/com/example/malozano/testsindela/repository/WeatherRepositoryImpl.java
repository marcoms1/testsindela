package com.example.malozano.testsindela.repository;

import android.util.Log;

import com.example.malozano.testsindela.model.ResponseWeather;
import com.example.malozano.testsindela.service.ConnectionApiService;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;

/**
 *
 *
 * Created by malozano on 06/06/2018.
 */

public class WeatherRepositoryImpl implements WeatherRepository {


    ConnectionApiService connectionApiService;


    public WeatherRepositoryImpl(ConnectionApiService connectionApiService) {
        this.connectionApiService = connectionApiService;
    }
    @Override
    public Observable<ResponseWeather> getCatalogos(String query) {
        return connectionApiService.getWeather(query,"json");
    }
}
