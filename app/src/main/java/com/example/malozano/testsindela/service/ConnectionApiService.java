
package com.example.malozano.testsindela.service;


import com.example.malozano.testsindela.model.ResponseWeather;
import com.example.malozano.testsindela.utils.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 *
 * The services calls
 * Created by malozano on 08/08/2017.
 */

public interface ConnectionApiService {

    @GET("v1/public/yql")
    Observable<ResponseWeather> getWeather(@Query("q") String query,@Query("format") String format);

}
