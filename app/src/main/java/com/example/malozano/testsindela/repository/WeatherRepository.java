package com.example.malozano.testsindela.repository;

import com.example.malozano.testsindela.model.ResponseWeather;

import rx.Observable;

/**
 * Created by malozano on 06/06/2018.
 */

public interface WeatherRepository {
    Observable<ResponseWeather> getCatalogos(String query);

}
